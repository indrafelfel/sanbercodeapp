import React from 'react'
import { StyleSheet, Text, View, Image, LayoutAnimation } from 'react-native'
import Indra from './indraputra.jpg';

export default function Biodata() {
    return (
        <>
        <View style={styles.bagianatas}>
            <Image style={styles.gambar} source={Indra} />
            <Text style={styles.title}>Indra Putra</Text>
        </View>
        <View style={styles.biowrapper}>
            <View style={styles.biodata}>
                <Text>Tanggal lahir</Text>
                <Text>25 Oktober 1976</Text>
            </View>
            <View style={styles.biodata}>
                <Text>Jenis Kelamin</Text>
                <Text>Laki-laki</Text>
            </View>
            <View style={styles.biodata}>
                <Text>Hobi</Text>
                <Text>Ngoding</Text>
            </View>
            <View style={styles.biodata}>
                <Text>No. Telp</Text>
                <Text>08127013869</Text>
            </View>
            <View style={styles.biodata}>
                <Text>Email</Text>
                <Text>indra_eva2002@yahoo.com</Text>
            </View>
            
        </View>
        </>

    )
}

const styles = StyleSheet.create({
    bagianatas: {backgroundColor: '#03b6fc', height: 240, alignItems: 'center', justifyContent: 'center'},
    title: {color: 'white', fontWeight: 'bold', paddingTop: 20},
    gambar: {width: 80, height: 80, borderRadius: 80 / 2},
    biodata: {flexDirection: 'row', justifyContent: 'space-between', marginVertical: 9},
    biowrapper: {backgroundColor: 'white',
                 marginHorizontal: 32,
                 padding: 20,
                 borderRadius: 10,
                 elevation: 8,
                 marginBottom: 16,
                 marginTop: -30,
    },
})
